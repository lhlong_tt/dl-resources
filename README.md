# dl-resources

Papers, Notes, Courses, Awesome DL

# Papers

### Sequence Models

- [GRU (Gated Recurrent Unit)](https://arxiv.org/pdf/1409.1259.pdf) and [here](https://arxiv.org/pdf/1412.3555.pdf)

- [LSTM](http://www.bioinf.jku.at/publications/older/2604.pdf) and [blog - Understand LSTM](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

- [Bidirectional RNN](https://pdfs.semanticscholar.org/4b80/89bc9b49f84de43acc2eb8900035f7d492b2.pdf)

### Convolutional Neural Network


- [Network In Network](https://arxiv.org/pdf/1312.4400v3.pdf)
This paper give you some notation about 1x1 Conv. This architechture is very important for CNN model.
Explaining about 1x1 Conv: [blog](https://iamaaditya.github.io/2016/03/one-by-one-convolution/)

- [OverFeat: Integrated Recognition, Localization and Detection using Convolutional Networks](https://arxiv.org/pdf/1312.6229.pdf)

- [Going deeper with convolutions](https://arxiv.org/pdf/1409.4842.pdf)
Have you hear about Inception model? This paper will [explain](https://hacktilldawn.com/2016/09/25/inception-modules-explained-and-implemented/) for you.

### Recurrent Neural Network

#### GRU
- [Learning Phrase Representations using RNN Encoder-Decoder for Statistical Machine Translation](https://arxiv.org/pdf/1406.1078.pdf)
- [On the Properties of Neural Machine Translation: Encoder-Decoder Approaches](https://arxiv.org/abs/1409.1259)
- [Empirical Evaluation of Gated Recurrent Neural Networks on Sequence Modeling](https://arxiv.org/abs/1412.3555v1)
- [A Theoretically Grounded Application of Dropout in Recurrent Neural Networks](https://arxiv.org/abs/1512.05287)

#### LSTM
- [Long short-term memory (original 1997 paper)](http://www.bioinf.jku.at/publications/older/2604.pdf)
- [Supervised sequence labeling with recurrent neural networks](http://www.cs.toronto.edu/~graves/preprint.pdf)
- [A Theoretically Grounded Application of Dropout in Recurrent Neural Networks](https://arxiv.org/abs/1512.05287)
- [Convolutional LSTM Network: A Machine Learning Approach for
Precipitation Nowcasting](http://arxiv.org/abs/1506.04214v1)

**Must-read**

- [Distributed Representations of Words and Phrases and their Compositionality](https://arxiv.org/pdf/1310.4546.pdf)
- [Sequence to Sequence Learning with Neural Networks](http://papers.nips.cc/paper/5346-sequence-to-sequence-learning-with-neural-networks.pdf)
- 

#### Normalization

- [Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift](https://arxiv.org/abs/1502.03167)


### Generative Models

- [Improved techniques for GANs](https://arxiv.org/pdf/1606.03498.pdf)
> give some tricks and tips for GANs, read more at [medium blog](https://towardsdatascience.com/gan-ways-to-improve-gan-performance-acf37f9f59b)

### Activation functions
> The list of all activation function can be found at [here](https://keras.io/activations/) and some [Advanced activation functions](https://keras.io/layers/advanced-activations/)
- [ELU](https://arxiv.org/abs/1511.07289) and [blog](https://medium.com/@krishnakalyan3/introduction-to-exponential-linear-unit-d3e2904b366c)

### Optimizers
- [RMSProp: Divide the gradient by a running average of its recent magnitude](http://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf)
- [Adaptive Subgradient Methods for Online Learning and Stochastic Optimization](http://www.jmlr.org/papers/volume12/duchi11a/duchi11a.pdf)
- [Adam: A Method for Stochastic Optimization](https://arxiv.org/abs/1412.6980v8)

### Need to classifier

- VGG model: [Very Deep Convolutional Networks for Large-Scale Image Recognition](https://arxiv.org/abs/1409.1556)
- ResNet: [Deep Residual Learning for Image Recognition](https://arxiv.org/abs/1512.03385)
- Inception [Rethinking the Inception Architecture for Computer Vision](https://arxiv.org/abs/1512.00567) and v4 [Inception-v4, Inception-ResNet and the Impact of Residual Connections on Learning](https://arxiv.org/abs/1602.07261)
- MobileNets [MobileNets: Efficient Convolutional Neural Networks for Mobile Vision Applications](https://arxiv.org/pdf/1704.04861.pdf) and MobileNets V2 [MobileNetV2: Inverted Residuals and Linear Bottlenecks](https://arxiv.org/abs/1801.04381)
- DenseNet [Densely Connected Convolutional Networks](https://arxiv.org/abs/1608.06993)
- NASNet [Learning Transferable Architectures for Scalable Image Recognition](https://arxiv.org/abs/1707.07012)

- Good blog 
> * [https://www.anishathalye.com/](https://www.anishathalye.com/)


# Notes
- [Cheatsheet AI](https://startupsventurecapital.com/essential-cheat-sheets-for-machine-learning-and-deep-learning-researchers-efb6a8ebd2e5)

- [Learning Rate Schedule](https://towardsdatascience.com/learning-rate-scheduler-d8a55747dd90), more at [this blog](https://machinelearningmastery.com/using-learning-rate-schedules-deep-learning-models-python-keras/)


# Courses


# Awesome DL

- [Awesome list for deep learning beginners](https://github.com/floodsung/Deep-Learning-Papers-Reading-Roadmap)
- [Awesome deep vision](https://github.com/kjw0612/awesome-deep-vision)

- List of [best CITE](https://scholar.google.com/citations?hl=en&vq=eng_artificialintelligence&view_op=list_hcore&venue=eqYFflc_uhEJ.2018) from Google scholars.

# Examples

- [Keras examples](https://github.com/keras-team/keras/tree/master/examples)